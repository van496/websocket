package com.zf.websocketdemo.wordUtil;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * prodData
 *
 * @author fan.zhou
 * @date 2022/7/27
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("prodData实体")
public class prodData {

    @ApiModelProperty(value = "item")
    private String item;

    @ApiModelProperty(value = "month")
    private String month;

    @ApiModelProperty(value = "year")
    private String year;

    @ApiModelProperty(value = "day")
    private String day;

    @ApiModelProperty(value = "monthPlan")
    private String monthPlan;

    @ApiModelProperty(value = "monthFinish")
    private String monthFinish;
}
