package com.zf.websocketdemo.wordUtil;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.data.*;
import com.deepoove.poi.data.style.PictureStyle;
import com.deepoove.poi.data.style.Style;
import com.deepoove.poi.plugin.table.LoopColumnTableRenderPolicy;
import com.deepoove.poi.plugin.table.LoopRowTableRenderPolicy;
import com.deepoove.poi.xwpf.WidthScalePattern;
import com.deepoove.poi.xwpf.XWPFHighlightColor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * word转化工具
 *
 * @author zhf
 * @date 2022/7/27
 */
@Slf4j
public class WordUtil {



    /**
     * 导出word
     * @param templateStream word模板文件流
     * @param response 响应
     * @param filename 文档名称
     * @param texts 文字
     * @param tables 表格
     * @param charts 图表
     * @param images 图片
     * @author zhf
     *  2022/7/27
     * @return boolean
     */
    public static void exportWord(InputStream templateStream, HttpServletResponse response, String filename,
                                  List<WordText> texts, List<WordTable> tables, List<WordChart> charts, List<WordImg> images){
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            String fileName = URLEncoder.encode(filename, "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".docx");
            createWord(templateStream, response.getOutputStream(), texts, tables, charts,images);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成word 包含 字符串 表格 图表
     * @param templateStream word模板文件流
     * @param outputStream word输出文件流
     * @param texts 文字
     * @param tables 表格
     * @param charts 图表
     * @param images 图片
     * @author zhf
     *  2022/7/27
     * @return boolean
     */
    public static OutputStream createWord(InputStream templateStream, OutputStream outputStream,
                                          List<WordText> texts, List<WordTable> tables, List<WordChart> charts, List<WordImg> images){
        if(ObjectUtil.isNotNull(templateStream)){
            //渲染文件
            XWPFTemplate compile = XWPFTemplate.compile(templateStream);
            //数据
            Map<String, Object> datas = new HashMap<String, Object>();
            //装载文字
            if(CollectionUtil.isNotEmpty(texts)){
                renderText(datas,texts);
            }
            //装载表格
            if(CollectionUtil.isNotEmpty(tables)){
                //获得配置信息
                renderTables(compile,datas,tables);
            }
            //渲染图表
            if(CollectionUtil.isNotEmpty(charts)){
                renderChart(datas,charts);
            }

            //渲染图片
            if(CollectionUtil.isNotEmpty(images)){
                renderImage(datas,images);
            }
            //生成word
            compile.render(datas);
            try {
                //写入输出流
                compile.write(outputStream);

            } catch (IOException e) {
//                e.printStackTrace();
                log.info("{}",e);
                //异常关闭数据流
                try {
                    //关闭输出流
                    if(outputStream!=null){
                        outputStream.close();
                    }
                } catch (IOException ex) {
//                    ex.printStackTrace();
                    log.info("{}",ex);
                }
            }finally {
                try {
                    //关闭输入流
                    templateStream.close();
                } catch (IOException e) {
//                    e.printStackTrace();
                    log.info("{}",e);
                }
            }
        }
        //返回
        return outputStream;
    }

    /**
     * 装载图表
     * @param datas 要生成word的 数据
     * @param charts 图表集合
     * @author zhf
     *  2022/7/27
     */
    private static void renderChart(Map<String, Object> datas, List<WordChart> charts) {
        for (WordChart wordChart:charts) {
            //横坐标
            String[] categories = wordChart.getCategories().toArray(new String[]{});
            //多系列图表
            //定义表头 横坐标
            ChartMultiSeriesRenderData chart = Charts.
                    ofMultiSeries(wordChart.getChartTitle(), categories).create();
            //放入系列
            List<SeriesRenderData> seriesRenderData=new ArrayList<>();
            for (WordChart.Series series :wordChart.getSeries()) {

                //字符串转BigDecimal
                List<String> seriesData = series.getSeriesData();
                List<BigDecimal> bigDecimals=new ArrayList<>();
                seriesData.stream().forEach(t-> {
                    bigDecimals.add(new BigDecimal(t));
                });
                BigDecimal[] bigDecimalArr = bigDecimals.toArray(new BigDecimal[]{});

                //系列名称、系列值
                SeriesRenderData seriesRender= new SeriesRenderData(series.getName(),bigDecimalArr);
                seriesRenderData.add(seriesRender);
            }
            //多系列
            chart.setSeriesDatas(seriesRenderData);

            //放入图表
            datas.put(wordChart.getTagName(),chart);
        }
    }


    /**
     * 装载表格
     * @param datas 要生成word的 数据
     * @param tables 表格集合
     * @author zhf
     *  2022/7/27
     */
    private static void renderTables(XWPFTemplate compile,Map<String, Object> datas, List<WordTable> tables) {
        for (WordTable wordTable:tables) {
                //动态行
            if(StringUtils.equals(wordTable.getLoop(),WordTable.ROW)){
                compile.getConfig().customPolicy(wordTable.getTagName(),new LoopRowTableRenderPolicy());
            }else if(StringUtils.equals(wordTable.getLoop(),WordTable.COLUMN)){
                //动态列
                compile.getConfig().customPolicy(wordTable.getTagName(),new LoopColumnTableRenderPolicy());
            }
            //填入表格数据
            datas.put(wordTable.getTagName(),wordTable.getData());
        }
    }


    /**
     * 装载文字
     * @param datas 要生成word的 数据
     * @param texts 文字集合
     * @author zhf
     *  2022/7/27
     */
    private static void renderText(Map<String, Object> datas, List<WordText> texts) {
        for (WordText textObj:texts) {
            TextRenderData textRender=null;
            //新段落 增加首行缩进、换行符
            if(textObj.getNewLine()){
                textObj.setText(new StringBuffer("   ").append(textObj.getText()).append("\n"));
            }
            //样式不为空 使用该样式
            if(ObjectUtil.isNotNull(textObj.getStyle())){
                textRender=new TextRenderData(textObj.getText().toString(), textObj.getStyle());
            }else{
                //默认
                textRender=new TextRenderData("000000", textObj.getText().toString());
            }
            datas.put(textObj.getTagName(),textRender);
        }

    }


    /**
     * 装载文字
     * @param datas 要生成word的 数据
     * @param images 图片集合
     * @author zhf
     *  2022/7/27
     */
    private static void renderImage(Map<String, Object> datas, List<WordImg> images) {
        for (WordImg wordImg:images) {
            //图片
            PictureRenderData image=null;
            if(StringUtils.isNotEmpty(wordImg.getOfBase64())){
                image=Pictures.ofBase64(wordImg.getOfBase64(),PictureType.JPEG).create();
            }else if(StringUtils.isNotEmpty(wordImg.getSvg())){
                datas.put(wordImg.getTagName(),wordImg.getSvg());
                continue;
            }else if(StringUtils.isNotEmpty(wordImg.getOfLocal())){
                image=Pictures.ofLocal(wordImg.getOfLocal()).create();
            }else if(StringUtils.isNotEmpty(wordImg.getOfUrl())){
                image=Pictures.ofUrl(wordImg.getOfUrl()).create();
            }else if(ObjectUtil.isNotNull(wordImg.getOfStream())){
                image=Pictures.ofStream(wordImg.getOfStream()).create();
            }
            else if(ObjectUtil.isNotNull(wordImg.getOfBufferedImage())){
                image=Pictures.ofBufferedImage(wordImg.getOfBufferedImage(),PictureType.JPEG).create();
            }

            if(ObjectUtil.isNotNull(image)){
                //图片样式
                PictureStyle style=new PictureStyle();
                //宽、高
                if(ObjectUtil.isNotNull(wordImg.getHeight())&&ObjectUtil.isNotNull(wordImg.getWidth())){
                    style.setHeight(wordImg.getHeight());
                    style.setWidth(wordImg.getWidth());
                }else {
                    //自适应
                    style.setScalePattern(WidthScalePattern.FIT);
                }
                image.setPictureStyle(style);
                //图片类型
                if(StringUtils.isNotEmpty(wordImg.getSvg())){
                    //svg
                    image.setPictureType(PictureType.SVG);
                }else if(ObjectUtil.isNotNull(wordImg.getPictureType())){
                    //指定图片类型
                    image.setPictureType(wordImg.getPictureType());
                }else{
                    //默认 JPEG
                    image.setPictureType(PictureType.JPEG);
                }

                datas.put(wordImg.getTagName(),image);
            }

        }

    }

    public static void main(String[] args) throws Exception {
        //模板地址
        String template = "C:\\Users\\Lenovo\\Desktop\\template.docx";
        //生成文件的保存地址
        String destFile = "C:\\Users\\Lenovo\\Desktop\\test.docx";
        //数据
        Map<String, Object> datas = new HashMap<String, Object>();

        //折线图
        ChartMultiSeriesRenderData chart = Charts
                .ofMultiSeries("进销气量对比图", new String[] { "1日", "2日", "3日", "4日", "5日", "6日", "7日", "8日", "9日", "10日" })
                .addSeries("总进气量", new BigDecimal[] { new BigDecimal("49.0"), new BigDecimal("99.0"),new BigDecimal("15.0"),new BigDecimal("39.0"),new BigDecimal("223.0"),new BigDecimal("169.0"),new BigDecimal("123.0"),new BigDecimal("235.0"),new BigDecimal("523.0"),new BigDecimal("567.0")})
                .addSeries("总供气量", new BigDecimal[] { new BigDecimal("22.0"), new BigDecimal("890.0"),new BigDecimal("823.0"), new BigDecimal("230.0"),new BigDecimal("83.0"), new BigDecimal("46.0"),new BigDecimal("103.0"), new BigDecimal("336.0"), new BigDecimal("423.0"), new BigDecimal("119.0")})
                .create();

        datas.put("lineChart", chart);

        //动态列表-行循环策略
        LoopRowTableRenderPolicy policy = new LoopRowTableRenderPolicy();
        Configure config = Configure.builder()
                .bind("list", policy).build();
        List<prodData> list =new ArrayList<>();
        for (int i=0;i<30;i++) {
            prodData data= prodData.builder().item("ABC1").month("ABC2").year("ABC3").day("ABC4").monthPlan("ABC5").monthFinish("ABC6").build();
            list.add(data);
        }
        datas.put("list",list);

        //文本 文本换行使用 \n 字符。
        //文本1
        StringBuffer text= new StringBuffer("\n").append("   本月共完成一般作业/工艺操作**项，维检修作业**项，其中包含重大维检修作业**项。").append("\n")
                .append("本月共完成一般作业。");
        Style style= Style.builder().build();
        //删除线
        style.setStrike(false);
        //粗体
        style.setBold(false);
        //斜体
        style.setItalic(false);
        //颜色
        style.setColor("000000");
        //下划线
        style.setUnderlinePatterns(UnderlinePatterns.NONE);
        //字体
        style.setFontFamily("仿宋_GB2312");
        //字号
        style.setFontSize(16);
        //背景高亮色
        style.setHighlightColor(XWPFHighlightColor.NONE);
        //上标或者下标
        style.setVertAlign("superscript");
        //间距
        style.setCharacterSpacing(20);
        TextRenderData textRender= new TextRenderData(text.toString(), style);
        TextRenderData textRender1= new TextRenderData("000000", text.toString());
        datas.put("text",textRender1);
        datas.put("text1",textRender1);

        //渲染文件
        XWPFTemplate compile = XWPFTemplate.compile(template,config);
        compile.render(datas);
        //输出为文件，指定输出文件名
        compile.writeToFile(destFile);
    }
}
