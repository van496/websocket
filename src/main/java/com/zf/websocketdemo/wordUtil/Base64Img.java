package com.zf.websocketdemo.wordUtil;

import org.springframework.util.ClassUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;

/**
 * @author fan.zhou
 * @description base64图片工具
 * @date 2021/2/20
 */
public class Base64Img {


    /**
     * @author fan.zhou
     * @description 图片转base64
     */
    public static String image2Base64Str(String filePath)
    {
        //将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        InputStream in = null;
        byte[] data = null;
        //读取图片字节数组
        try
        {
            in = new FileInputStream(filePath);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);//返回Base64编码过的字节数组字符串
    }

    /**
     * @author fan.zhou
     * @description base64转图片
     */
    public static boolean base64Str2Image(String base64Str,String imgFilePath)
    {
        //对字节数组字符串进行Base64解码并生成图片
        if (base64Str == null) //图像数据为空
            return false;
        BASE64Decoder decoder = new BASE64Decoder();
        try
        {
            //Base64解码
            byte[] b = decoder.decodeBuffer(base64Str);
            for(int i=0;i<b.length;++i)
            {
                if(b[i]<0)
                {//调整异常数据
                    b[i]+=256;
                }
            }
            //生成jpeg图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /**
     * @author fan.zhou
     * @description base64转字节
     */
    public static byte[] base64Str2Byte(String base64Str)
    {
        //对字节数组字符串进行Base64解码并生成图片
        if (base64Str == null) //图像数据为空
            return null;
        BASE64Decoder decoder = new BASE64Decoder();
        try
        {
            //Base64解码
            byte[] b = decoder.decodeBuffer(base64Str);
            return b;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * @author fan.zhou
     * @description base64转字节
     */
    public static String byte2Base64Str(byte[] bytes)
    {
        //对字节数组字符串进行Base64解码并生成图片
        if (bytes == null) //图像数据为空
            return null;
        try
        {
            //对字节数组Base64编码
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(bytes);//返回Base64编码过的字节数组字符串
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * @author fan.zhou
     * @description 存文件到本地目录
     * @date 2021/3/26
     * @param imgBase64
     * @param fileDir
     * @return boolean
     */
    public static File saveImgtoLocal(String imgBase64,String imgName,String fileDir){
        String path = ClassUtils.getDefaultClassLoader().getResource("static").getPath();
        String comLogoPath= path+ File.separator+fileDir;
        File filePath= new File(comLogoPath);
        if(!filePath.exists()){
            filePath.mkdirs();
        }
        imgName= imgName+".jpg";
        String imgFilePath = comLogoPath+File.separator+imgName;
        boolean save= base64Str2Image(imgBase64, imgFilePath);
        if(save){
            return new File(imgFilePath);
        }else{
            return null;
        }
    }
}
