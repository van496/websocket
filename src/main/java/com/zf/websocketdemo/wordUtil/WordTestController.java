package com.zf.websocketdemo.wordUtil;


import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 生产周月报
 *
 * @author zhf
 * @date 2022-07-11
 */
@RestController
@Validated
@Api(tags = "生产周月报-周报")
@ApiSupport(author = "zhf")
@RequestMapping("/WordTest")
public class WordTestController {


    /**
     * 查询单条数据
     *
//     * @param dataId 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne")
    public void export(HttpServletResponse response) throws Exception {

        //模板
        String template ="C:\\Users\\10501\\Desktop\\template.docx";

        //生成文件地址
        String outpath ="C:\\Users\\10501\\Desktop\\text.docx";

        File file= new File(template);
        FileInputStream fs= new FileInputStream(file);
        //文字
        WordText text1=WordText.builder().
                tagName("text1")
                .text(new StringBuffer("本月各场站/阀室运行情况，目前管网的运行方式，是否发生非计划停气、停机等事件。")).build();
        WordText text2=WordText.builder().
                tagName("text2").newLine(true)
                .text(new StringBuffer("本月共完成一般作业/工艺操作**项，维检修作业**项，其中包含重大维检修作业**项。")).build();
        List<WordText> texts =new ArrayList<>();
        texts.add(text1);
        texts.add(text2);
        //表格
        List<prodData> list =new ArrayList<>();
        for (int i=0;i<30;i++) {
            prodData data= prodData.builder().item("ABC1").month("ABC2").year("ABC3").day("ABC4").monthPlan("ABC5").monthFinish("ABC6").build();
            list.add(data);
        }
        WordTable wordTable=WordTable.builder().loop(WordTable.ROW).tagName("list").data(list).build();
        List<WordTable> tables =new ArrayList<>();
        tables.add(wordTable);

        //图表
        List<WordChart> charts =new ArrayList<>();
        List<String> categories= Arrays.asList(new String("1日,2日,3日,4日,5日,6日,7日,8日,9日,10日").split(","));
        WordChart wordChart=WordChart.builder().tagName("lineChart").chartTitle("进销气量对比图").categories(categories).build();
        //系列
        List<String> seriesData1= Arrays.asList(new String("49.0,99.0,15.0,39.0,223.0,169.0,123.0,235.0,523.0,567.0").split(","));
        WordChart.Series series1=wordChart.new Series("总进气量",seriesData1);

        List<String> seriesData2= Arrays.asList(new String("22.0,890.0,823.0,230.0,83.0,46.0,103.0,336.0,423.0,119.0").split(","));
        WordChart.Series series2=wordChart.new Series("总供气量",seriesData2);
        List<WordChart.Series> series=new ArrayList<>();
        series.add(series1);
        series.add(series2);
        wordChart.setSeries(series);
        charts.add(wordChart);

        String excelName="test";
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(excelName, "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".docx");
        WordUtil.createWord(fs,response.getOutputStream(),texts,tables,charts,null);
    }
}
