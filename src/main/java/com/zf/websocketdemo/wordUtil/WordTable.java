package com.zf.websocketdemo.wordUtil;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * word 表格
 *
 * @author zhf
 * @date 2022/7/27
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("word 表格")
public class WordTable {

    //行循环
    public static String COLUMN = "COLUMN";

    //列循环
    public static String ROW = "ROW";

    /**
     * word模板标签
     */
    @ApiModelProperty(value = "word模板标签")
    private String tagName;

    /**
     * 数据集合 List对象
     * 模板中的表格标签与集合中字段需一一对应
     */
    @ApiModelProperty(value = "数据集合 List对象")
    private Object data;

    /**
     * 动态表格循环方式
     * COLUMN 行循环
     * ROW 列循环
     */
    @Builder.Default
    @ApiModelProperty(value = "动态表格循环方式")
    private String loop = ROW;

}
