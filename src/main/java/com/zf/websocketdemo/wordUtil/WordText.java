package com.zf.websocketdemo.wordUtil;

import com.deepoove.poi.data.style.Style;
import com.deepoove.poi.xwpf.XWPFHighlightColor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;

/**
 * word 文字
 *
 * @author zhf
 * @date 2022/7/27
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("word 文字")
public class WordText {

    //默认样式
   private static Style defaulStyle=Style.builder().build();
    static {
        //删除线
        defaulStyle.setStrike(false);
        //粗体
        defaulStyle.setBold(false);
        //斜体
        defaulStyle.setItalic(false);
        //颜色 16进制色号
        defaulStyle.setColor("000000");
        //下划线
        defaulStyle.setUnderlinePatterns(UnderlinePatterns.NONE);
        //字体
        defaulStyle.setFontFamily("仿宋_GB2312");
        //字号
        defaulStyle.setFontSize(16);
        //背景高亮色
        defaulStyle.setHighlightColor(XWPFHighlightColor.NONE);
        //上标或者下标
        defaulStyle.setVertAlign("superscript");
        //间距
        defaulStyle.setCharacterSpacing(2);
    }

    /**
     * word模板标签
     */
    @ApiModelProperty(value = "word模板标签")
    private String tagName;


    /**
     * 文字内容,\n换行
     */
    @ApiModelProperty(value = "文字内容,\n换行")
    private StringBuffer text;

    /**
     * 是否新段落
     * 是 自动增加 段落前空格 和 换行符
     * 否 纯字符
     */
    @Builder.Default
    @ApiModelProperty(value = "是否新段落")
    private Boolean newLine=false;

    /**
     * 样式
     * 1默认不设置,会跟随模板文字样式
     * 2可以自定义:
     *    Strike删除线
     *    Bold粗体
     *    Italic斜体
     *    Color颜色
     *    UnderlinePatterns下划线
     *    FontFamily字体
     *    FontSize字号
     *    HighlightColor背景高亮色
     *    VertAlign上标或者下标
     *    CharacterSpacing间距
     */
    @Builder.Default
    @ApiModelProperty(value = "样式")
    private Style style=null;

}
