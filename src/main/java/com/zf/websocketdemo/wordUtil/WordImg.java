package com.zf.websocketdemo.wordUtil;

import com.deepoove.poi.data.PictureType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 * WordImg
 *
 * @author zf
 * @date 2022/7/29
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("word 图片")
public class WordImg {

    /**
     * word模板标签
     */
    @ApiModelProperty(value = "word模板标签")
    private String tagName;

    /**
     * 图片宽度
     */
    @ApiModelProperty(value = "图片宽度")
    private Integer width;

    /**
     * 图片高度
     */
    @ApiModelProperty(value = "图片高度")
    private Integer height;

    /**
     * 图片类型
     */
    @ApiModelProperty(value = "base64图片")
    private PictureType pictureType;

    /**
     * svg 网络图
     */
    @ApiModelProperty(value = "svg 网络图")
    private String svg;

    /**
     * 图片本地路径
     */
    @ApiModelProperty(value = "图片本地路径")
    private String ofLocal;

    /**
     * 网络图片地址
     */
    @ApiModelProperty(value = "网络图片地址")
    private String ofUrl;


    /**
     * base64图片
     */
    @ApiModelProperty(value = "base64图片")
    private String ofBase64;

    /**
     * 图片流
     */
    @ApiModelProperty(value = "图片流")
    private InputStream ofStream;

    /**
     * java图片
     */
    @ApiModelProperty(value = "java图片")
    private BufferedImage ofBufferedImage;

}
