package com.zf.websocketdemo.wordUtil;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * word 图表
 *
 * @author zhf
 * @date 2022/7/27
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("word 图表")
public class WordChart  {

    /**
     * word模板标签
     */
    @ApiModelProperty(value = "word模板标签")
    private String tagName;


    /**
     * 图表标题
     */
    @ApiModelProperty(value = "图表标题")
    private String chartTitle;

    /**
     * 横坐标
     */
    @ApiModelProperty(value = "横坐标")
    private List<String> categories;

    /**
     * 图表系列
     */
    @ApiModelProperty(value = "图表系列")
    private List<Series> series;


    /**
     * 图表统计项
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel("word 图表系列")
    public class Series{
        /**
         * 图表系列名称
         */
        @ApiModelProperty(value = "图表系列名称")
        private String name;

        /**
         * 图表系列数据集
         */
        @ApiModelProperty(value = "图表系列数据集")
        private List<String> seriesData;
    }

}
