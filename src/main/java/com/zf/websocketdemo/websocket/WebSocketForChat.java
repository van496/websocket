package com.zf.websocketdemo.websocket;


import com.alibaba.fastjson.JSON;
import com.zf.websocketdemo.util.MessageUtil;
import com.zf.websocketdemo.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * WebSocketForChat
 *
 * @author fan.zhou
 * @date 2022/4/9
 */
//声名websocket名称 与前端对应
//配置configurator 获取Httpsession 中信息
@Component
@Slf4j
@ServerEndpoint(value = "/websocketZ",configurator = HttpSessionConfigurator.class)
public class WebSocketForChat /* 方法1-继承  extends Endpoint*/ {


    /*
     方法1-继承
     @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {

    }*/

    //定义全局websocket session
    private  Session websocketSession;

    //定义浏览器登录后session
    private  HttpSession httpSession;

    //保存当前系统HttpSession 和其中的webSockekt信息
    private static Map<HttpSession,WebSocketForChat> olineUsers =new HashMap<>();

    //当前登录用户总数
    private static int onlineUserNum=0;

    /**
     * @author fan.zhou
     *  前端第一次请求websocket 触发，打开一个会话
     * @date 2022/4/9
     * @return void
     */
    @OnOpen
    public void OnOpen(Session session, EndpointConfig endpointConfig){
        //1 初始化websocketsession
        this.websocketSession=session;
        //2 获取当前用户信息 httpsession信息
        HttpSession httpSession=(HttpSession)endpointConfig.getUserProperties().get(HttpSession.class.getName());
        this.httpSession=httpSession;
        log.info("当前登录用户>>{},endpoint实例>>{}",httpSession.getAttribute("username"),hashCode());
        //3  记录当前系统HttpSession 和其中的webSockekt信息
        if(StringUtils.isNotNull(httpSession.getAttribute("username"))){
            olineUsers.put(httpSession,this);
            addOnlineNum();//记录用户数量
        }
        //4 获取当前所有登录用户
        String users=getUsers();
        //5 广播消息 {"data": "HEIMA,Deng, ITCAST", "toName":"", "fromName":"", "type":"user"}
        String message = MessageUtil.getContent(MessageUtil.TYPE_USER, "", "", users);
        broadCastMessage(message);

    }

    /**
     * @author fan.zhou
     * 接收消息
     * @date 2022/4/10
     * @param message 消息
     * @param session 会话
     * @return void
     */
    @OnMessage
    public void OnMessage(String message,Session session){
        log.info("OnMessage 当前登陆人>{},接收信息>{}",httpSession.getAttribute("username"),message);
        //1 获取客户端的消息并解析 {＂fromName＂：＂Deng＂， ＂toName＂：＂HEIMA＂，＂content＂：＂约会呀”｝
        Map<String,String> map = JSON.parseObject(message, Map.class);
        String fromName =map.get("fromName");
        String toName =map.get("toName");
        String content =map.get("content");
        //2 判定是否有接收人
        if(StringUtils.isEmpty(toName)){
            return;
        }
        //3 接收人是否广播(all), 全部发送
        String msg = MessageUtil.getContent(MessageUtil.TYPE_MESSAGE, fromName, toName, content);
        log.info("服务端给客户端发送消息，消息内容:{}",msg);
        if(toName.equals("all")){
            broadCastMessage(msg);
        }else{
            //4 不是all,指定发送
            sendSingleMsg(msg,fromName,toName);
        }

    }

    /**
     * @author fan.zhou
     * 关闭
     * @date 2022/4/10
     * @param
     * @return void
     */
    @OnClose
    public void onClose(Session session, CloseReason closeReason){
        subOnlineNum();
        log.info("客户端关闭了一个链接，当前在线认数 {}",getOnlineNum());
    }

    /**
     * @author fan.zhou
     * 异常
     * @date 2022/4/10
     * @param
     * @return void
     */
    @OnError
    public void onError(Session session, Throwable throwable) {
        throwable.printStackTrace();
        log.info("服务异常");
    }

    /**
     * @author fan.zhou
     * 广播消息
     * @date 2022/4/10
     * @param
     * @return java.lang.String
     */
    private void broadCastMessage(String message) {
        if(olineUsers.size()>0){
            for (Map.Entry<HttpSession,WebSocketForChat> entry: olineUsers.entrySet()) {
                try {
                    entry.getValue().websocketSession.getBasicRemote().sendText(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @author fan.zhou
     * 广播消息
     * @date 2022/4/10
     * @param
     * @return java.lang.String
     */
    private void sendSingleMsg(String message,String fromName,String toName) {

        //1 判断接收人是否在线

        if(olineUsers.size()>0){
            for (Map.Entry<HttpSession,WebSocketForChat> entry: olineUsers.entrySet()) {
                try {
                    //1 判断接收人是否在线
                    if(entry.getKey().getAttribute("username").equals(toName)||entry.getKey().getAttribute("username").equals(fromName)){
                        entry.getValue().websocketSession.getBasicRemote().sendText(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @author fan.zhou
     * 获取登录的所有用户
     * @date 2022/4/10
     * @param
     * @return java.lang.String
     */
    private String getUsers() {
        List<String> users=new ArrayList<>();
        if(olineUsers.size()>0){
            for (Map.Entry<HttpSession,WebSocketForChat> entry: olineUsers.entrySet()) {
                String username = (String)entry.getKey().getAttribute("username");
                users.add(username);
            }
        }
       return StringUtils.join(users,",");
    }

    /**
     * @author fan.zhou
     * 获取当前用户
     * @date 2022/4/10
     * @return void
     */
    public int getOnlineNum(){
        return onlineUserNum;
    }

    /**
     * @author fan.zhou
     * 当前在线用户增加
     * @date 2022/4/10
     * @return void
     */
    public synchronized void addOnlineNum(){
        onlineUserNum++;
    }

    /**
     * @author fan.zhou
     * 当前在线用户减少
     * @date 2022/4/10
     * @return void
     */
    public synchronized void subOnlineNum(){
        onlineUserNum-- ;
    }
}
