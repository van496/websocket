package com.zf.websocketdemo.websocket;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * HttpSessionConfigurator 继承ServerEndpointConfig内部类
 *
 * @author fan.zhou
 * @date 2022/4/10
 */
@Component
public class HttpSessionConfigurator extends ServerEndpointConfig.Configurator {

    /**
     * @author fan.zhou
     * 覆写握手过程中信息
     * @date 2022/4/10
     * @param conf
     * @param request
     * @param response
     * @return void
     */
    @Override
    public void modifyHandshake(ServerEndpointConfig conf, HandshakeRequest request, HandshakeResponse response) {
        //获取httpsession
        HttpSession httpSession=(HttpSession)request.getHttpSession();
        //将httpsession 赋值到conf中
        conf.getUserProperties().put(HttpSession.class.getName(),httpSession);
    }
}
