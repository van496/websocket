package com.zf.websocketdemo.opencvtest.service;

import cn.hutool.core.comparator.CompareUtil;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class OpencvTestServiceImpl {




    private final String fileFolder="D:\\workspace\\project\\websocket\\opencvImg";

    private final String faceIdCfgXml="opencv/haarcascade_frontalface_alt.xml";

    private final BigDecimal similarPass=BigDecimal.valueOf(0.72);

    private CascadeClassifier faceDetector;

    /**
     * 对比两个图片文件
     * @param file1
     * @param file2
     * @return
     */
    public Boolean comPare(MultipartFile file1, MultipartFile file2) {
        // 加载OpenCV库
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        //文件存本地
        File imgFile1 =getFile(file1);
        File imgFile2 =getFile(file2);
        Mat image1 = Imgcodecs.imread(imgFile1.getAbsolutePath());
        Mat image2 = Imgcodecs.imread(imgFile2.getAbsolutePath());
        // 将图片处理成一样大
        Imgproc.resize(image1, image1, image2.size());
//        Imgproc.resize(image2, image2, image1.size());
        // 计算直方图
         double similarity = calculateSimilarity(image1, image2);
        System.out.println("图片相似度(直方图): " + similarity);
        //相似度>98
        if(CompareUtil.compare(BigDecimal.valueOf(similarity),similarPass)>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     *
     * @param file1
     * @param file2
     * @return
     */
    public Boolean comPare1(MultipartFile file1, MultipartFile file2) {
        //文件存本地
        File imgFile1 =getFile(file1);
        File imgFile2 =getFile(file2);
        //加载opencv
        initOpencv();
        //获取面部图片
        File faceFile1=getImgFace(imgFile1);
        File faceFile2=getImgFace(imgFile2);
        //比较图片
        return comPareFace(faceFile1,faceFile2,imgFile1,imgFile2);
    }

    /**
     * 加载opencv
     */
    private void initOpencv() {
        // 加载 OpenCV 库
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        // 加载人脸分类器
        ClassPathResource classPathResource = new ClassPathResource(faceIdCfgXml);
        try {
            String filePth=ResourceUtils.getFile("classpath:").getAbsolutePath()+File.separator+classPathResource.getPath();
            faceDetector = new CascadeClassifier(filePth);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 对比两个图片文件
     * @param faceFile1
     * @param faceile2
     * @param orFile1
     * @param orFile2
     * @return
     */
    public Boolean comPareFace(File faceFile1, File faceile2,File orFile1, File orFile2) {
        // 加载OpenCV库
//        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        Mat mat_1 = conv_Mat(faceFile1);
        Mat mat_2 = conv_Mat(faceile2);
        Mat hist_1 = new Mat();
        Mat hist_2 = new Mat();

        //颜色范围
        MatOfFloat ranges = new MatOfFloat(0f, 256f);
        //直方图大小， 越大匹配越精确 (越慢)
        MatOfInt histSize = new MatOfInt(5000);

        Imgproc.calcHist(Arrays.asList(mat_1), new MatOfInt(0), new Mat(), hist_1, histSize, ranges);
        Imgproc.calcHist(Arrays.asList(mat_2), new MatOfInt(0), new Mat(), hist_2, histSize, ranges);
        // CORREL 相关系数
        double res = Imgproc.compareHist(hist_1, hist_2, Imgproc.CV_COMP_CORREL);
        System.out.println("图片相似度(直方图): " + res);
        //相似度>98
//        delFile(faceFile1, faceile2, orFile1, orFile2);
        if(CompareUtil.compare(BigDecimal.valueOf(res),similarPass)>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 删除文件
     * @param faceFile1
     * @param faceile2
     * @param orFile1
     * @param orFile2
     */
    private  void delFile(File faceFile1, File faceile2, File orFile1, File orFile2) {
        faceFile1.delete();
        faceile2.delete();
        orFile1.delete();
        orFile2.delete();
    }

    /**
     * 获取文件
     * @param file1
     * @return
     */
    private File getFile(MultipartFile file1) {
        File fileForder=new File(fileFolder);
        if(!fileForder.exists()){
            fileForder.mkdirs();
        }
        File localFile=new File(fileForder,file1.getOriginalFilename());
        try {
//            localFile.createNewFile();
            file1.transferTo(localFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return localFile;
    }

    private static double calculateSimilarity(Mat image1, Mat image2) {
        // 计算直方图
        Mat hist1 = calculateHistogram(image1);
        log.info("image1 直方图值>>{}",hist1);
        Mat hist2 = calculateHistogram(image2);

        log.info("image2 直方图值>>{}",hist2);
        // 计算相似度
        final double similarity = Imgproc.compareHist(hist1, hist2, Imgproc.CV_COMP_CORREL);
        log.info("image1,image2 直方图值相似度>>{}",similarity);
        // 手动释放内存
//        if (hist1 != null) {
//            hist1.release();
//        }
//        if (hist2 != null) {
//            hist2.release();
//        }
        return similarity;
    }

    private static Mat calculateHistogram(Mat image) {
        Mat hist = new Mat();

        // 设置直方图参数
        MatOfInt histSize = new MatOfInt(256);
        MatOfFloat ranges = new MatOfFloat(0, 256);
        MatOfInt channels = new MatOfInt(0);
        List<Mat> images = new ArrayList<>();
        images.add(image);
        // 计算直方图
        Imgproc.calcHist(images, channels, new Mat(), hist, histSize, ranges);
        return hist;
    }



    /**
     * 获取人脸图片
     * @param file
     */
    public File  getImgFace(File file){
        // 读取图像
        Mat image = Imgcodecs.imread(file.getAbsolutePath());
        // 3 特征匹配
        MatOfRect face = new MatOfRect();
        faceDetector.detectMultiScale(image, face);
      // 4 匹配 Rect 矩阵 数组
      Rect[] rects = face.toArray();
      System.out.println("匹配到 " + rects.length + " 个人脸");
      String facePath=fileFolder+File.separator+file.getName().split("\\.")[0]+"_face.jpg";
      // 5 为每张识别到的人脸画一个圈
      for (Rect rect : face.toArray()) {
          Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                  new Scalar(0, 255, 0), 3);
          imageCut(file.getAbsolutePath(), facePath, rect.x, rect.y, rect.width, rect.height);// 进行图片裁剪
      }
      return new File(facePath);
//      // 6 展示图片
//      HighGui.imshow("人脸识别", image);
//      HighGui.waitKey(0);
    }

    /**
     *
     * @param imagePath
     * @param outFile
     * @param posX
     * @param posY
     * @param width
     * @param height
     */
    public static void imageCut(String imagePath, String outFile, int posX, int posY, int width, int height) {
        // 原始图像
        Mat image = Imgcodecs.imread(imagePath);
        // 截取的区域：参数,坐标X,坐标Y,截图宽度,截图长度
        Rect rect = new Rect(posX, posY, width, height);
        // 两句效果一样
        Mat sub = image.submat(rect); // Mat sub = new Mat(image, rect);
        Mat mat = new Mat();
        Size size = new Size(width, height);
        Imgproc.resize(sub, mat, size);// 将人脸进行截图并保存
        Imgcodecs.imwrite(outFile, mat);
        System.out.println(String.format("图片裁切成功，裁切后图片文件为： %s", outFile));
    }

    /**
     * 灰度化人脸
     *
     * @param imgFile
     * @return
     */
    public Mat conv_Mat(File imgFile) {

        Mat image0 = Imgcodecs.imread(imgFile.getAbsolutePath());

        Mat image1 = new Mat();
        // 灰度化
        Imgproc.cvtColor(image0, image1, Imgproc.COLOR_BGR2GRAY);
        // 探测人脸
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image1, faceDetections);
        // rect中人脸图片的范围
        for (Rect rect : faceDetections.toArray()) {

            Mat face = new Mat(image1, rect);
            return face;
        }
        return null;
    }



}
