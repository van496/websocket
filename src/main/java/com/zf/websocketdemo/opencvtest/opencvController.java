package com.zf.websocketdemo.opencvtest;


import com.zf.websocketdemo.controller.BaseController;
import com.zf.websocketdemo.model.AjaxResult;
import com.zf.websocketdemo.opencvtest.service.OpencvTestServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@RequestMapping("/opencv")
public class opencvController extends BaseController {

    @Resource
    private  OpencvTestServiceImpl opencvTestService;

    @PostMapping("/opcvImgC")
    AjaxResult testImgCmp(@RequestParam("file1") MultipartFile file1,@RequestParam("file2") MultipartFile file2){
       Boolean result=opencvTestService.comPare1(file1,file2);
       return  AjaxResult.success(result);
    }
}
