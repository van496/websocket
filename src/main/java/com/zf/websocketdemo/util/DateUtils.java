package com.zf.websocketdemo.util;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 * 
 * @author zhhs
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", 
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     * 获取当前Date型日期
     * 
     * @return Date() 当前日期
     */
    public static Date getNowDate()
    {
        return new Date();
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     * 
     * @return String
     */
    public static String getDate()
    {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String getTime()
    {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    public static final String dateTimeNow()
    {
        return dateTimeNow(YYYYMMDDHHMMSS);
    }

    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }

    public static final String dateTime(final Date date)
    {
        return parseDateToStr(YYYY_MM_DD, date);
    }

    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    public static final Date dateTime(final String format, final String ts)
    {
        try
        {
            return new SimpleDateFormat(format).parse(ts);
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }

    /**
     * 日期路径 即年/月/日 如20180808
     */
    public static final String dateTime()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyyMMdd");
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate()
    {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算相差天数
     */
    public static int differentDaysByMillisecond(Date date1, Date date2)
    {
        return Math.abs((int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24)));
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }
    /**
     * 给定时间是否在今天之前或等于今天
     */
    public static boolean afterCurrentTime(Date date) {
        long currentTime = parseDate(getDate()).getTime();
        long time = parseDate(parseDateToStr(YYYY_MM_DD, date)).getTime();
        if (currentTime == time) {
            return true;
        }
        if (currentTime - time > 0) {
            return true;
        }
        return false;
    }

    /**
     * 给定时间是否在今天之前
     * @param date
     * @return
     */
    public static boolean afterTime(Date date){
        long currentTime = parseDate(getDate()).getTime();
        long time = parseDate(parseDateToStr(YYYY_MM_DD, date)).getTime();
        if (currentTime - time > 0) {
            return true;
        }
        return false;
    }

    /**
     * 日期是否有效  true 有效    false 无效
     */
    public static boolean dateIsEffective(Date date) {
        return !afterCurrentTime(date);
    }

    /**
     * 获取明天的时间
     */
    public static Date getNextDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, +1);
        return calendar.getTime();
    }

    /**
     * 当前时间添加上指定天数的日期
     */
    public static Date addDays(int howManyDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, howManyDays);
        return calendar.getTime();
    }


    /**
     * 当前时间加上指定秒数
     */
    public static Date addSecond(int second) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.SECOND, second);
        return calendar.getTime();
    }

    /**
     * 指定时间添加上指定天数的日期
     */
    public static Date addDays(Date startTime, int howManyDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        calendar.add(Calendar.DAY_OF_MONTH, howManyDays);
        return calendar.getTime();
    }


    /**
     * 指定时间添加上指定天数的日期
     */
    public static String addDay(Date startTime, int howManyDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        calendar.add(Calendar.DAY_OF_MONTH, howManyDays);
        Date time = calendar.getTime();
        return parseDateToStr(YYYY_MM_DD,time);
    }



    /**
     * 两个日期是否相同
     */
    public static boolean equalDate(Date date1, Date date2) {
        long time1 = parseDate(parseDateToStr(YYYY_MM_DD, date1)).getTime();
        long time2 = parseDate(parseDateToStr(YYYY_MM_DD, date2)).getTime();
        if (time1 == time2) {
            return true;
        }
        return false;
    }


    /**
     *@Author wf
     *@Date
     * 当前时间前多少天
     */
    public static String getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String result = format.format(today);
        return result;
    }


    public static void main(String[] args) {
        //Date date = parseDate("2021-03-09");


        String startTime = "2021-03-09";
        String endTime = "2021-03-15";
        int i = DateUtils.differentDaysByMillisecond(DateUtils.parseDate(startTime), DateUtils.parseDate(endTime));

        System.out.println(DateUtils.parseDate(startTime));

        String s = addDay(parseDate(startTime), 2);
        System.out.println(s);


        /*String date2 = DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD, DateUtils.addDays(30));
        System.out.println(date2);*/
    }

}
