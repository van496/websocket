package com.zf.websocketdemo.controller;

import com.google.code.kaptcha.Constants;
import com.zf.websocketdemo.model.AjaxResult;
import com.zf.websocketdemo.util.ServletUtils;
import com.zf.websocketdemo.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录验证
 * 
 * @author zhhs
 */
@Controller
public class LoginController extends BaseController
{

    /**
     * @author fan.zhou
     * 登录页面
     * @date 2022/4/9
     * @param request
     * @param response
     * @return java.lang.String
     */
    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response)
    {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request))
        {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }

        return "login";
    }

    /**
     * @author fan.zhou
     * 登录
     * @date 2022/4/9
     * @param username
     * @param password
     * @param rememberMe
     * @return com.zf.websocketdemo.model.AjaxResult
     */
    @PostMapping("/login")
    @ResponseBody
    public AjaxResult ajaxLogin(String username, String password, String validateCode, Boolean rememberMe)
    {
        String code =(String) ServletUtils.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if(StringUtils.isNotEmpty(username)&&StringUtils.isNotEmpty(password)&&StringUtils.isNotEmpty(validateCode)&&code.equals(validateCode)){
            ServletUtils.getSession().setAttribute("username",username);
            return success();
        }else if(StringUtils.isNotEmpty(validateCode)&&!code.equals(validateCode)){
            return error("验证码错误");
        }else{
            return error("登陆失败");
        }

    }

    /**
     * @author fan.zhou
     * 首页
     * @date 2022/4/9
     * @return java.lang.String
     */
    @GetMapping("/index")
    public String index()
    {
        return "index";
    }

    /**
     * @author fan.zhou
     * 聊天页
     * @date 2022/4/9
     * @param modelMap
     * @return java.lang.String
     */
    @GetMapping("/chat")
    public String chat(ModelMap modelMap)
    {
        Object username = ServletUtils.getSession().getAttribute("username");
        modelMap.put("username",username);
        return "chat";
    }
}
