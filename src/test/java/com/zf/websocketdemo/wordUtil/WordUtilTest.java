package com.zf.websocketdemo.wordUtil;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class WordUtilTest extends WordUtil {

    File template =new File("C:\\Users\\Lenovo\\Desktop\\template.docx");

    String file="C:\\Users\\Lenovo\\Desktop\\demo.png";

    /**
     * @author zf
     * 测试base64
     *  2022/7/30
     * @param
     * @return void
     */
    @Test
    public void testCreateImage_base64() throws FileNotFoundException {

        String base64 = Base64Img.image2Base64Str(file);
        WordImg wordImg= WordImg.builder().ofBase64(base64).tagName("base64").height(200).width(400).build();
        List<WordImg> imgList= new ArrayList<>();
        imgList.add(wordImg);
        FileInputStream inputStream=new FileInputStream("C:\\Users\\Lenovo\\Desktop\\template.docx");
        FileOutputStream fileOutputStream=new FileOutputStream("C:\\Users\\Lenovo\\Desktop\\base64.docx");
        WordUtil.createWord(inputStream,fileOutputStream,null,null,null,imgList);

    }

    /**
     * @author zf
     *
     *  2022/7/30
     * @param
     * @return void
     */
    @Test
    public void testCreateImage_Stream() throws FileNotFoundException {
        FileInputStream imageStream=new FileInputStream(file);
        FileInputStream templateStream=new FileInputStream(template);
        FileOutputStream outPutStream=new FileOutputStream("C:\\Users\\Lenovo\\Desktop\\stream.docx");
        WordImg wordImg=WordImg.builder().tagName("stream").ofStream(imageStream).build();
        List<WordImg> imgList= new ArrayList<>();
        imgList.add(wordImg);
        WordUtil.createWord(templateStream,outPutStream,null,null,null,imgList);
    }

    @Test
    public void testCreateImage_BufferedImage() throws IOException {
        FileInputStream templateStream =new FileInputStream(template);
        FileOutputStream outputStream=new FileOutputStream("C:\\Users\\Lenovo\\Desktop\\bfImage.docx");
        File file = new File(this.file);
        BufferedImage bfImage = createBfImage(file);
        WordImg wordImg=WordImg.builder().tagName("bfImage").ofBufferedImage(bfImage).build();
        WordImg[] arr=new WordImg[]{wordImg};
        List<WordImg> wordImgs = Arrays.asList(arr);
        WordUtil.createWord(templateStream,outputStream,null,null,null,wordImgs);
    }

    /**
     * @author zf
     * 绘图
     *  2022/7/30
     * @param file
     * @return java.awt.image.BufferedImage
     */
    private BufferedImage createBfImage(File file) throws IOException {
        //原图片
        BufferedImage image= ImageIO.read(file);
        BufferedImage bufferedImage=new BufferedImage(image.getWidth(),image.getHeight(),BufferedImage.TYPE_INT_RGB);
        bufferedImage.createGraphics().drawImage(image,0,0,null);
        //获取透明图片、写字
        Graphics2D graphics = bufferedImage.createGraphics();
        Font font= new Font("仿宋_GB2312",Font.PLAIN,20);
        graphics.setFont(font);
        graphics.setColor(Color.BLACK);
        //抗锯齿
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.drawString("台湾NO.1( •̀ ω •́ )y 2b吗?q(≧▽≦q)",80,200);
        //拼入原图
        graphics.dispose();

        return  bufferedImage;
    }
}